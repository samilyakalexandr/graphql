import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorResolver } from './author.resolver';
import { AuthorService } from './author.service';
import { AuthorRepository } from './author.repository';

@Module({
  imports: [TypeOrmModule.forFeature([AuthorRepository])],
  providers: [AuthorResolver, AuthorService]
})
export class AuthorModule {}
