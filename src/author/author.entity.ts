import {Column, Entity, PrimaryGeneratedColumn, ManyToMany, JoinTable} from 'typeorm';
import {Book} from "../book/book.entity";
import {ArgsType, Field, ID, ObjectType} from '@nestjs/graphql';

@Entity()
@ArgsType()
@ObjectType()
export class Author {
    @Field(() => ID)
    @PrimaryGeneratedColumn()
    id: number;

    @Field(() => String)
    @Column()
    firstName: string;

    @Field(() => String)
    @Column()
    lastName: string;

    @Field(() => [Book])
    @ManyToMany(type => Book, book => book.author)
    book: Promise<Book[]>;
}

