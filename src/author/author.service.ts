import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Author } from './author.entity';
import { AuthorRepository } from './author.repository';
import { BookRepository } from '../book/book.repository';
import { Book } from '../book/book.entity';
import { getConnection } from "typeorm";

@Injectable()
export class AuthorService {
    constructor(
        @InjectRepository(AuthorRepository)
        private readonly authorRepo: AuthorRepository,
        @InjectRepository(AuthorRepository)
        private readonly bookRepo: BookRepository,

    ) {}

    async getAuthor(id: number): Promise<Author> {

       const result = await this.authorRepo.findOne({
            where: { id }
        });
        console.log(result, "result");
        console.log(result.id, "result");
       return result
    }

    async getAuthors(minNumberOfBooks: number, maxNumberOfBooks: number): Promise<Author[]> {
        return this.authorRepo
            .createQueryBuilder("author")
            .leftJoin("author.book", "book")
            .groupBy("author.id")
            .having(minNumberOfBooks ? "COUNT(book.id) >= :minNumberOfBooks": '1=1', { minNumberOfBooks })
            .andHaving(maxNumberOfBooks ? "COUNT(book.id) <= :maxNumberOfBooks": '1=1', { maxNumberOfBooks })
            .getMany();
    }

    async createAuthor( firstName: string , lastName: string): Promise<Author> {
        return await this.authorRepo.save({
            firstName,
            lastName,
        });
    }
    async deleteAuthor(id: number) {
        const sucesso = await this.authorRepo.delete({id});
        return sucesso.affected;
    }
    async deleteAuthorWithBooks(id: number) {

        const author = await getConnection().manager.findOne(Author, id);
        console.log(author, 'author')
        const manyBooks = await getConnection()
            .createQueryBuilder()
            .relation(Author, "book")
            .of(author)
            .loadMany();
         const manyBooksIds = manyBooks.map(el => el.id );

        let manyLonelyBooksIds = await Promise.all(manyBooksIds.map(async el => {
             const elAuthors = await this.bookRepo
                     .createQueryBuilder("author")
                     .leftJoinAndSelect("author.book", "book")
                     .where("book.id = :ids", { ids: el })
                     .getMany()

                if( elAuthors.length < 2 ) return el
        }
        ));

        manyLonelyBooksIds = manyLonelyBooksIds.filter( Boolean )
         await getConnection()
            .createQueryBuilder()
            .relation(Author, "book")
            .of(id)
            .remove(manyBooks);

        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(Author)
            .where("id = :id", { id })
            .execute();
    if(manyLonelyBooksIds.length) {
        await getConnection()
            .createQueryBuilder()
            .delete()
            .from(Book)
            .where("id IN (:...ids)", {ids: manyLonelyBooksIds})
            .execute();
        }
        return manyLonelyBooksIds.length + manyBooks.length + (author ? 1 : 0);
    }
}
