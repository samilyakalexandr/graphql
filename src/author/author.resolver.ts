import { Author } from './author.entity';
import {Args, Mutation, ID, Query, Resolver, Int} from '@nestjs/graphql';
import { AuthorService } from './author.service';
import { AllAuthorsArgs } from './args/allAuthorsArgs';
import { AuthorInput } from './input/authroInput'

@Resolver('Author')
export class AuthorResolver {
    constructor(private readonly authorService: AuthorService) {}

    @Query(() => String)
    async hello() {
        return 'hello world';
    }

    @Query(() => Author, {nullable: true})
    async getAuthor(@Args('id', {type: () => ID}) id: number): Promise<Author> {
        return this.authorService.getAuthor(id);
    }

    @Query(() => [Author])
    async getAuthors(@Args() { minNumberOfBooks, maxNumberOfBooks }: AllAuthorsArgs): Promise<Author[]> {
        return this.authorService.getAuthors(minNumberOfBooks, maxNumberOfBooks);
    }

    @Mutation(() => Author)
    async createAuthor(
        @Args('input') input: AuthorInput,
    ): Promise<Author> {
        return this.authorService.createAuthor(input.firstName, input.lastName);
    }

    @Mutation(() => Int)
    async deleteAuthor(@Args('id', {type: () => ID}) id: number)
        {
            return this.authorService.deleteAuthor(id);
        }

    @Mutation(() => Int)
    async deleteAuthorWithBooks(@Args('id', {type: () => ID}) id: number)
    {
        return this.authorService.deleteAuthorWithBooks(id);
    }

}