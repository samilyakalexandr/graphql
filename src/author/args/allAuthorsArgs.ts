import {ArgsType, Field, Int} from '@nestjs/graphql';

@ArgsType()
export class AllAuthorsArgs {
    @Field(() => Int, { nullable: true })
    minNumberOfBooks?: number;

    @Field(() => Int, { nullable: true })
    maxNumberOfBooks?: number;
}