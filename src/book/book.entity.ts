import { Column, Entity, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm';
import {Author} from "../author/author.entity";
import {InputType, ObjectType, Field, Int, ID, ArgsType} from '@nestjs/graphql';

@Entity()
@ArgsType()
@ObjectType()
export class Book {
    @Field(() => ID)
    @PrimaryGeneratedColumn()
    id: number;

    @Field(() => String)
    @Column()
    title: string;

    @Field(() => [Author])
    @JoinTable()
    @ManyToMany(type => Author, author => author.book)
    author: Promise<Author[]>;
}
