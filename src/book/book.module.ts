import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookResolver } from './book.resolver';
import { BookService } from './book.service';
import { BookRepository } from './book.repository';
import {AuthorRepository} from "../author/author.repository";

@Module({
  imports: [TypeOrmModule.forFeature([BookRepository, AuthorRepository])],
  providers: [BookResolver, BookService]
})
export class BookModule {}
