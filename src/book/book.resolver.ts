import { Book } from './book.entity';
import {Args, ID, Int, Mutation, Query, Resolver} from '@nestjs/graphql';
import { BookService } from './book.service';
import { BookInput } from '../book/input/bookInput';


@Resolver('Book')
export class BookResolver {
    constructor(private readonly bookService: BookService) {}

    @Query(() => Book,{nullable: true})
    async getBook(@Args('id', {type: () => ID}) id: number): Promise<Book> {
        return this.bookService.getBook(id);
    }

    @Query(() => [Book])
    async getBooks(@Args( 'title', { type: () => String,
        nullable: true}) title: string): Promise<Book[]> {
        return this.bookService.getBooks(title);
    }

    @Mutation(() => Book)
    async createBook(
        @Args('input') input: BookInput,
    ): Promise<Book> {
        return this.bookService.createBook(input.title, input.authorId);
    }

    @Mutation(() => Book)
    async addAuthor(@Args('bookId', {type: () => ID}) bookId: number, @Args('authorId', { type: () => ID }) authorId: number): Promise<Book> {
        return this.bookService.addAuthor(bookId, authorId);
    }

    @Mutation(() => Int)
    async deleteBook(@Args('id', {type: () => ID}) id: number)
    {
        return this.bookService.deleteBook(id);
    }

}