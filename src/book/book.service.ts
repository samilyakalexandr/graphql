import { Injectable } from '@nestjs/common';
import { InjectRepository} from '@nestjs/typeorm';
import { Book } from './book.entity';
import { BookRepository } from './book.repository';
import {In, Like, getConnection} from "typeorm";
import {AuthorRepository} from "../author/author.repository";

@Injectable()
export class BookService {
    constructor(
        @InjectRepository(BookRepository)
        private readonly bookRepo: BookRepository,

        @InjectRepository(AuthorRepository)
        private readonly authorRepo: AuthorRepository,
    ) {}

    async getBook(id: number): Promise<Book> {
        return await this.bookRepo.findOne({
            where: { id }
        });
    }

    async getBooks(title: string): Promise<Book[]> {
        const result = await this.bookRepo.find({
            title: Like(title? title: '%%')
           });
        return result
    }

    async createBook( title: string , authorId: [number] ): Promise<Book> {
      const resultAuthors = await this.authorRepo.find( { id : In(authorId) } );
      console.log(resultAuthors, "resultAuthors")
        const result = await this.bookRepo.save({
            title,
            author: Promise.resolve(resultAuthors),
        });
        return result
    }

    async addAuthor (bookId: number, authorId: number): Promise<Book> {
         await getConnection()
            .createQueryBuilder()
            .relation(Book, "author")
            .of(bookId)
            .add(authorId);

    const resultBook = await this.bookRepo.findOne( { id : bookId } );
        return resultBook
    }


    async deleteBook(id: number) {
        const sucesso = await this.bookRepo.delete({id});
        return sucesso.affected;
    }


}
