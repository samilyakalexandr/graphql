import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'mysql',
    host: 'host.docker.internal',
    port: 6000,
    username: 'user',
    password: 'password',
    database: 'db',
    entities: [__dirname + '/../**/*.entity{.ts,.js}'],
    synchronize: true,
};