import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import {join} from 'path';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorModule } from './author/author.module';
import { BookModule } from './book/book.module';
import { typeOrmConfig } from './config/typeOrmConfig';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile:(join(process.cwd(),'src/schema.gql'))
    }),
    TypeOrmModule.forRoot(typeOrmConfig),
    AuthorModule,
    BookModule
  ]
})
export class AppModule {}